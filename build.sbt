name := "gatling"

version := "0.1"

scalaVersion := "2.12.10"

enablePlugins(GatlingPlugin)

libraryDependencies += "io.gatling.highcharts" % "gatling-charts-highcharts" % "3.3.1" % "test"

libraryDependencies += "io.gatling"            % "gatling-test-framework"    % "3.3.1" % "test"
