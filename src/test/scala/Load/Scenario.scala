package Load

import Requests._
import io.gatling.core.scenario.Simulation
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

object Scenario {
  val UC_01_OpenStartPage = scenario("OpenStartPage")
    .pace(5 seconds)
    .exec(Requests.openStartPage)
  //.exec{ session => println(session); session } //Debug

  val UC_02_DeleteComputer = scenario("DeleteComputer")
    .pace(15 seconds)
    .exec(Requests.deleteComputer)
  //.exec{ session => println(session); session } //Debug

  val UC_03_CreateNewComputer = scenario("CreateNewComputer")
    .pace(10 seconds)
    .exec(Requests.createNewComputer)
  //.exec{ session => println(session); session } //Debug
}
