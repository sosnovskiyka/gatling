package Load

import Requests._
import TestPlan._
import Scenario._
import io.gatling.core.scenario.Simulation
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class LoadScript extends Simulation{

  val httpConf = http
    .baseUrl("http://computer-database.gatling.io/computers")
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip, deflate, br")
    .acceptLanguageHeader("ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3")
    .userAgentHeader("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0")

  //Для теста надежности
  /*setUp(
    UC_01_OpenStartPage.inject(TestPlan.StabTest(0, 10, 30, 60)),
    UC_02_DeleteComputer.inject(TestPlan.StabTest(0, 1, 30, 60)),
    UC_03_CreateNewComputer.inject(TestPlan.StabTest(0, 10, 30, 60))
  ).protocols(httpConf).maxDuration(2 minutes)*/

  //Для теста на поиск максперф
  setUp(
    UC_01_OpenStartPage.inject(TestPlan.MaxPerf(5, 5, 5, 30, 10)),
    UC_02_DeleteComputer.inject(TestPlan.MaxPerf(1, 1, 5, 30, 10)),
    UC_03_CreateNewComputer.inject(TestPlan.MaxPerf(2, 3, 5, 30, 10))
  ).protocols(httpConf).maxDuration(5 minutes)
}


