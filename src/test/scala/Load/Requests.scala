package Load

import java.text.SimpleDateFormat
import java.util.{Calendar, UUID}
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

object Requests {
  val feeder = csv("data.csv").circular

  val openStartPage =
    exec(
      http("Open start page")
        .get("")
        .headers(Map("Accept" -> "*/*"))
        .check(status.is(200))
    )

  val createNewComputer =
    exec(
      http("openStartPage")
        .get("")
        .check(status.is(200))
    ).pause(1, 2)
    .exec(
      http("pressButtonAddNewComputer")
        .get("/new")
        .check(status.is(200))
        .check(regex("<option value=\"(.+?)\"").findRandom.saveAs("company"))
    ).pause(1, 3)
    .exec(session =>
      session
        .set("randomNameComputer", UUID.randomUUID.toString))
    .feed(feeder)
      .exec(
        http("createNewComputer")
          .post("")
          .formParam("name",	"${nameComputer}" + "${randomNameComputer}")
          .formParam("introduced", "2019-11-26")
          .formParam("discontinued", "2019-11-28")
          .formParam("company",	"${company}")
      )

  val deleteComputer =
      exec(
        http("openStartPage")
          .get("")
          .check(status.is(200))
          .check(regex("<a href=\"/computers/(.+?)\"").findRandom.saveAs("randomComp"))
      ).pause(1)
    .exec(
      http("openComp")
        .get("/${randomComp}")
        .check(status.is(200))
    ).pause(2)
    .exec(
      http("deleteComp")
        .post("/${randomComp}/delete")
    )
}
