package Load

import Requests._
import io.gatling.core.scenario.Simulation
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._

import scala.concurrent.duration._

object TestPlan {
  val stabTest = Seq(
    rampConcurrentUsers(0) to (10) during(10 seconds),
    constantConcurrentUsers(10) during(60 seconds)
  )

  val maxPerfTest = Seq(
    incrementConcurrentUsers (5)
      .times (5)
      .eachLevelLasting (60 seconds)
      .separatedByRampsLasting (10 seconds)
      .startingFrom (5)
  )

  def MaxPerf(incrementUsers: Int, startUsers:Int, step: Int, durationStep: Int, rampToStep: Int) = Seq(
    incrementConcurrentUsers (incrementUsers)
      .times (step)
      .eachLevelLasting (durationStep seconds)
      .separatedByRampsLasting (rampToStep seconds)
      .startingFrom (startUsers)
  )

  def StabTest(startUser: Int, finishUser: Int, rampUp: Int, duration: Int) = Seq(
    rampConcurrentUsers(startUser) to (finishUser) during(rampUp seconds),
    constantConcurrentUsers(finishUser) during(duration seconds)
  )
}
